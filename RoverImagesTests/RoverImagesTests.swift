//
//  RoverImagesTests.swift
//  RoverImagesTests
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import XCTest
@testable import Mars_Images

class RoverImagesTests: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()

        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.

    }

    func testEmptyPhotoCollection() {
        let empty = ""
        let result = MarsRoverAPI.photoCollection(fromJSON: empty.data(using: .utf8)!)
        
        switch result {
        case .failure:
            let a = "Passed"
            XCTAssert( a == "Passed")
        case .endOfPhotos:
            XCTFail()
        case .success(_):
            XCTFail()
        }
    }
    func testPhotoCollectionReachedLastPage() {
        let lastPageJson = """
                  {"photos":[]}
                  """
        let result = MarsRoverAPI.photoCollection(fromJSON: lastPageJson.data(using: .utf8)!)
        
        switch result {
        case .failure:
            XCTFail()
        case .endOfPhotos:
            let a = "Passed"
            XCTAssert( a == "Passed")
        case .success(_):
            XCTFail()
        }
    }
    
    func testPhotoCollectionWithTwoPhotos() {
        let twoPhotosJson = """
        {"photos":[{"id":494587,"sol":100,"camera":{"id":22,"name":"MAST","rover_id":5,"full_name":"Mast Camera"},"img_src":"http://mars.jpl.nasa.gov/msl-raw-images/msss/00100/mcam/0100MR0006610020104364I01_DXXX.jpg","earth_date":"2012-11-16","rover":{"id":5,"name":"Curiosity","landing_date":"2012-08-06","launch_date":"2011-11-26","status":"active","max_sol":2540,"max_date":"2019-09-28","total_photos":366206,"cameras":[{"name":"FHAZ","full_name":"Front Hazard Avoidance Camera"},{"name":"NAVCAM","full_name":"Navigation Camera"},{"name":"MAST","full_name":"Mast Camera"},{"name":"CHEMCAM","full_name":"Chemistry and Camera Complex"},{"name":"MAHLI","full_name":"Mars Hand Lens Imager"},{"name":"MARDI","full_name":"Mars Descent Imager"},{"name":"RHAZ","full_name":"Rear Hazard Avoidance Camera"}]}},{"id":494588,"sol":100,"camera":{"id":22,"name":"MAST","rover_id":5,"full_name":"Mast Camera"},"img_src":"http://mars.jpl.nasa.gov/msl-raw-images/msss/00100/mcam/0100MR0006610010104363I01_DXXX.jpg","earth_date":"2012-11-16","rover":{"id":5,"name":"Curiosity","landing_date":"2012-08-06","launch_date":"2011-11-26","status":"active","max_sol":2540,"max_date":"2019-09-28","total_photos":366206,"cameras":[{"name":"FHAZ","full_name":"Front Hazard Avoidance Camera"},{"name":"NAVCAM","full_name":"Navigation Camera"},{"name":"MAST","full_name":"Mast Camera"},{"name":"CHEMCAM","full_name":"Chemistry and Camera Complex"},{"name":"MAHLI","full_name":"Mars Hand Lens Imager"},{"name":"MARDI","full_name":"Mars Descent Imager"},{"name":"RHAZ","full_name":"Rear Hazard Avoidance Camera"}]}}]}
        """
        
        
        let result = MarsRoverAPI.photoCollection(fromJSON: twoPhotosJson.data(using: .utf8)!)
        
        switch result {
        case .failure:
            XCTFail()
        case .endOfPhotos:
            XCTFail()
        case let .success(photos):
            XCTAssert(photos.count == 2)
            XCTAssert(photos[0].imageUrl == URL(string: "http://mars.jpl.nasa.gov/msl-raw-images/msss/00100/mcam/0100MR0006610020104364I01_DXXX.jpg"))
            XCTAssert(photos[1].imageUrl == URL(string: "http://mars.jpl.nasa.gov/msl-raw-images/msss/00100/mcam/0100MR0006610010104363I01_DXXX.jpg"))
        }
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            NasaClient().fetchPhotoBatch(page: 2, completionHandler: { (photosResult) in
                CollectionViewController().fetchCompleteHandler(with: photosResult)
            })
        }

}
}
