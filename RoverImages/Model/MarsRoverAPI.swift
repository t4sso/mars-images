//
//  MarsRoverAPI.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation

/// For later usage, to allow the user to select camera
enum Cam: String {
    case FHAZ = "fhaz"
    case RHAZ = "rhaz"
    case MAST = "mast"
    case CHEMCAM = "chemcam"
    case MAHLI = "mahli"
    case MARDI = "mardi"
    case NAVCAM = "navcam"
    case PANCAM = "pancam"
    case MINITES = "minites"
}



/// Methods to interact with the NASA API.
struct MarsRoverAPI {
    //TODO: add rovers to params and remove from baseURL so that we can pull images from all of them.
    private static let baseURL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos"
    
    // If my API Key gets blocked (1000 requests in 1hr), switch to DEMO key, and then back after an hour. Info on Rate limits: https://api.nasa.gov
    //private static let myAPIKey = "DEMO_KEY"
    private static let myAPIKey = "uET0CUnnq9rraYYO4S3IkgPhxHrPcI48nuhICwsY"
    
    
    /// We can add parameters to get different photo categories or different rovers here. As an example sol and camera are hardcoded here.
    /// - Parameter camera: which camera photos to get
    static func buildURL(camera: Cam, page: String = "1") -> URL {
        
        var urlComps = URLComponents(string: baseURL)!
        var items = [URLQueryItem]()
        let baseParams = [
            "api_key": myAPIKey,
            "sol": "100",
            "page": "\(String(describing: page))",
            "camera": camera.rawValue
        ]
        for (key, value) in baseParams {
            let item = URLQueryItem(name: key, value: value)
            items.append(item)
        }
        urlComps.queryItems = items
        
        return urlComps.url!
    }
    
    /// This calls the API and returns the result. similar functions could retrieve other info besides photos
    /// - Parameter data: the data returned from the dataTask
    static func photoCollection(fromJSON data: Data) -> NasaResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            
            guard let jsonDictionary = jsonObject as? [AnyHashable: Any],
                let photosArray = jsonDictionary["photos"] as? [[String: Any]] else {
                    return .failure(NasaError.wrongJson)
            }
            var photoResultArray = [NasaPhoto]()
            
            for photoJSON in photosArray {
                if let photo = nasaPhoto(fromJSON: photoJSON) {
                    photoResultArray.append(photo)
                }
            }
            if photoResultArray.isEmpty && photosArray.isEmpty {
                return .endOfPhotos
            }
            return .success(photoResultArray)
        } catch let error {
            return .failure(error)
        }
    }
    
    /// Create our photo properties, we can add more if needed here. Unwrap any needed properties and return NasaPhoto object
    /// - Parameter json: json returned from API call
    private static func nasaPhoto(fromJSON json: [String: Any]) -> NasaPhoto? {
        guard let photoID = json["id"] as? Int,
            let urlString = json["img_src"] as? String,
            let url = URL(string: urlString),
            let earthDate = json["earth_date"] as? String else {
                return nil
        }
        let photoIDAsString = String(photoID)
        return NasaPhoto(imageUrl: url, photoId: photoIDAsString, earthDate: earthDate)

    }
    
    
}
