//
//  NasaPhoto.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation

struct NasaPhoto: Codable {
    
    let imageUrl: URL
    let photoId: String
    let earthDate: String
   
    
    
}
