//
//  NasaPhotoStore.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation

final class NasaClient {
    private let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    
     func nasaPhotoProcess(data: Data?, error: Error?) -> NasaResult {
        guard let jsonData = data else { return .failure(error!) }
        return MarsRoverAPI.photoCollection(fromJSON: jsonData)
    }
    /// Use to receive 25 photo batch for the collectionView.
    /// - Parameter completionHandler:
    func fetchPhotoBatch(page: Int, completionHandler: @escaping (NasaResult) -> Void) {
        
        let url = MarsRoverAPI.buildURL(camera: .MAST, page: String(page))
        let request = URLRequest(url: url)
        //TODO: maybe check for connectivity here, NWPathmonitor or Reachability
        
        urlSession.dataTask(with: request) { (data, response, error) in
            let result = self.nasaPhotoProcess(data: data, error: error)
            DispatchQueue.main.async  { //add to main queue to display
                completionHandler(result)
                
            }
        }.resume()
    }
    
    
}

enum NasaResult {
    case success([NasaPhoto])
    case failure(Error)
    case endOfPhotos
   
}

enum NasaError: Error {
    case wrongJson
    case noPhotoInJson
    
}

