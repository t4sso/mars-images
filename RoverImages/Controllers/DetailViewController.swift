//
//  ViewController.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var image: NasaPhoto?
     
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = image?.photoId ?? ""
        topLabel.text = "Image ID: " + (image?.photoId ?? "")
        lowerLabel.text = "Taken on: " + (image?.earthDate ?? "")
        //TODO: titleLabel is hardcoded to Curiosity Mast Camera. Change if/when we add other rovers/cameras
        
        
        //MARK: Image loading implementation without caching.
        //        let data = try? Data(contentsOf: image?.img_src ?? URL(string:"http://empty.nasa.gov")!)
        //        if let imageData = data {
        //            imageView.image = UIImage(data: imageData)
        //        }
        //MARK: Using SDWebImage below for placeholder in case of slow net.
        imageView.sd_setImage(with: image?.imageUrl, placeholderImage: UIImage(named: "placeholder.png"))
                
    }
    
    
}


