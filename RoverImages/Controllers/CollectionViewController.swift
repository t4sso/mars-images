//
//  CollectionViewController.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController {
    
    var client: NasaClient!
    var photosToDisplay: [NasaPhoto] = []
    var currentPage: Int = 0
    var reachedLastPage = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Mars Photo Gallery"
        client = NasaClient()
        collectionView.dataSource = self
        if currentPage == 0 { client.fetchPhotoBatch(page: 1, completionHandler: { result in
            self.fetchCompleteHandler(with: result)
        })
            currentPage = 1
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        if( traitCollection.forceTouchCapability == .available){
            
            registerForPreviewing(with: self, sourceView: view)
        }
        // Do any additional setup after loading the view.
        
    }
    
    func fetchCompleteHandler(with result: NasaResult) {
        switch result {
        case let .failure(error):
            print("Error while fetching: \(error)")
            let alert = UIAlertController(title: "Couldn't fetch photos", message: "Unable to fetch the photos from the server. Are you connected to the internet? \nError: \(error)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            NSLog("User pressed OK")
            }))
            self.present(alert, animated: true, completion: nil)
        case .endOfPhotos:
            //Do stuff if there are no more photos coming from the API
            print("No more photos to receive")
            reachedLastPage = true
        case let .success(photos):
            self.photosToDisplay += photos
            print("Received batch. Total: \(self.photosToDisplay.count)")
            self.currentPage += 1
            
            
        }
        self.collectionView.reloadData()
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        guard let sender = sender as? Int else {
            return
        }
        if segue.identifier == "simpleSegue" {
            if let destination = segue.destination as? DetailViewController {
                destination.image = photosToDisplay[sender]
                print("sending image: \(photosToDisplay[sender].photoId)")
            }
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "simpleSegue", sender: indexPath.row)
        
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch reachedLastPage {
        case false: // while we don't know if there are subsequent pages, we assume there is one more element to load. Then, in collectionView(_:willDisplay:forItemAt) we pull another batch until we reach the final page.
            
            return self.photosToDisplay.count + 1
        case true:
            return self.photosToDisplay.count
        }
        
        
    }
    
    
    /// We use this while we haven't reached the final page to load another batch. First we check that we haven't yet reached the final page and we ask for one more batch if the index has reached the end of photosToDisplay.
    ///
    /// - Parameters:
    ///   - indexPath: see also comments in collectionView(_:numberOfItemsInSection:) for an explanation
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.photosToDisplay.count && !reachedLastPage {
            
            client.fetchPhotoBatch(page: currentPage, completionHandler: { (photosResult) in
                self.fetchCompleteHandler(with: photosResult)
            })
            
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mastImageCell", for: indexPath) as! CollectionViewCell

        // Configure the cell
        if photosToDisplay.count > indexPath.row {
            let photo = photosToDisplay[indexPath.row]
            cell.configure(with: photo)
        }
        return cell
    }
    
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
    
}

extension CollectionViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let locationCell : CGPoint = self.collectionView.convert(location, from: self.view)
        guard let indexPath = collectionView?.indexPathForItem(at: locationCell) else { return nil }
        guard let cell = collectionView?.cellForItem(at: indexPath) else { return nil }
        guard let detailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return nil }

        detailViewController.image = photosToDisplay[indexPath.row]
        detailViewController.preferredContentSize = CGSize(width: 0.0, height: 450)
        previewingContext.sourceRect = cell.frame

        return detailViewController
    }

    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
}
