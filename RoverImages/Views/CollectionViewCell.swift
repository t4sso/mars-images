//
//  CollectionViewCell.swift
//  RoverImages
//
//  Created by Tassos on 13/11/2019.
//  Copyright © 2019 Tassos Chouliaras. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bigLabel: UILabel!
    @IBOutlet weak var smallLabel: UILabel!
    
    
    var image: UIImage? {
        didSet {
            self.imageView.image = image
        }
    }
    
    /// Create the cell properties for the collectionView
    /// - Parameter photo: passed from the collectionView to use to set up the cell
    func configure(with photo: NasaPhoto?) {
        //MARK: Implementation without caching. It lags, because it loads 25 images from URLs sequentially.
        //        let data = try? Data(contentsOf: photo.img_src)
        //        if let imageData = data {
        //            let image = UIImage(data: imageData)
        //
        //cell.image = image
        //        }
        
        //MARK: Trying SDWebImage dependency to cache asynchronously and use placeholder image. Works much smoother.
        imageView.sd_setImage(with: photo?.imageUrl, placeholderImage: UIImage(named: "placeholder.png"))
       
        //MARK: Labels in the collectionView for possible future addition
        bigLabel.text = photo?.earthDate
        smallLabel.text = photo?.photoId
        bigLabel.isHidden = true
        smallLabel.isHidden = true
        
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 3
        
        
    }
}




